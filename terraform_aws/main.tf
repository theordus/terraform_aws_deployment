terraform {
    backend "s3" {
        bucket = "terraform-backend-b4"
        region = "eu-west-2"
        key = "terraform.tfstate"
    }
}


provider "aws" {   
    region = var.region
}

module "main_vpc" {
    source = "./modules/vpc"
    vpc_cidr = var.vpc_cidr
    project = var.project
    env = var.env
}

module "pri_sub1" {
    source = "./modules/subnet"
    vpc_id = module.main_vpc.vpc_id
    subnet_cidr = var.private_subnet_cidr1
    availability_zone = var.az1
    project = var.project
    env = var.env
    enable_public_ip = false
}

module "pri_sub2" {
    source = "./modules/subnet"
    vpc_id = module.main_vpc.vpc_id
    subnet_cidr = var.private_subnet_cidr2
    availability_zone = var.az2
    project = var.project
    env = var.env
    enable_public_ip = false
}

module "pub_sub" {
    source = "./modules/subnet"
    vpc_id = module.main_vpc.vpc_id
    subnet_cidr = var.public_subnet_cidr
    availability_zone = var.az1
    project = var.project
    env = var.env
    enable_public_ip = true
}